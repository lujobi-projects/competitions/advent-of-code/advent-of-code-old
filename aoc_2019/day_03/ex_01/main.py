# -*- coding: utf-8 -*-
import logging

import numpy as np
from utils.read_ipt import read_ipt

MAX_GRID_RADIUS = 5000


def create_grid(ipt):
    grid = np.zeros([2 * MAX_GRID_RADIUS + 1, 2 * MAX_GRID_RADIUS + 1], dtype=np.bool)
    row, col = MAX_GRID_RADIUS, MAX_GRID_RADIUS
    new_row, new_col = row, col
    for i in ipt.split(','):
        direction, value = i[0], int(i[1:])
        new_row += value if direction == 'D' else -value if direction == 'U' else 0
        new_col += value if direction == 'R' else -value if direction == 'L' else 0
        grid[min(row, new_row): max(row, new_row) + 1, min(col, new_col): max(col, new_col) + 1] = 1
        row, col = new_row, new_col
    return grid


def run():
    ipt = read_ipt(__file__)
    g1 = create_grid(ipt[0])
    g2 = create_grid(ipt[1])
    g1[MAX_GRID_RADIUS][MAX_GRID_RADIUS] = 0
    g2[MAX_GRID_RADIUS][MAX_GRID_RADIUS] = 0
    row_match, col_match = np.where(g1 * g2 == 1)
    min_dist = 5 * MAX_GRID_RADIUS
    for r, c in zip(row_match, col_match):
        min_dist = min(min_dist, abs(r - MAX_GRID_RADIUS) + abs(c - MAX_GRID_RADIUS))
    return min_dist

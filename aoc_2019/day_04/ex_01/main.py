# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    lower, upper = int(ipt[0].split('-')[0]), int(ipt[0].split('-')[1])
    tot = 0
    for nr in range(lower, upper + 1):
        crit_adj = False
        crit_incr = True
        nr = str(nr)
        prev = nr[0]
        for d in nr[1:]:
            crit_adj = crit_adj or prev == d
            crit_incr = crit_incr and int(prev) <= int(d)
            prev = d
        if crit_adj and crit_incr:
            tot += 1
    return tot

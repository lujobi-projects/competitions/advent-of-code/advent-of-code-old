# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    lower, upper = int(ipt[0].split('-')[0]), int(ipt[0].split('-')[1])
    tot = 0
    for nr in range(lower, upper + 1):
        crit_adj, crit_incr = False, True
        nr = str(nr)
        prev = nr[0]
        gs = 1
        for d in nr[1:]:
            if prev == d:
                gs += 1
            elif gs == 2:
                crit_adj = True
                gs = 1
            else:
                gs = 1
            crit_incr = crit_incr and int(prev) <= int(d)
            prev = d

        crit_adj = crit_adj or gs == 2
        if crit_adj and crit_incr:
            tot += 1
    return tot

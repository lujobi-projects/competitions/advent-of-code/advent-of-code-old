# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    total = 0
    for i in read_ipt(__file__, int):
        total += i // 3 - 2
    return total

# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def calc_fuel(i):
    return i // 3 - 2


def calc_additional_fuel(mass):
    total, fuel = 0, calc_fuel(mass)
    while fuel > 0:
        total += fuel
        fuel = calc_fuel(fuel)
    return total


def run():
    return sum([calc_additional_fuel(i) for i in read_ipt(__file__, int)])

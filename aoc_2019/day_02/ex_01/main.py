# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run_intcode(ipt):
    for i in range(0, len(ipt), 4):
        opcode = ipt[i]
        if opcode not in [1, 2, 99]:
            logging.info(f'error, unknown opcode {opcode} found at id {i}')
            return []
        elif opcode == 99:
            logging.info(f'halted at index {i}')
            break
        val1 = ipt[ipt[i + 1]]
        val2 = ipt[ipt[i + 2]]
        ipt[ipt[i + 3]] = val1 + val2 if opcode == 1 else val1 * val2
    return ipt


def run():
    line = read_ipt(__file__)[0]
    intcode = [int(x) for x in line.split(',')]

    intcode[1] = 12
    intcode[2] = 2
    res = run_intcode(intcode)
    return res[0]

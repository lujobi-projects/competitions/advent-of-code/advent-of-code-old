# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def reset_ipt():
    line = read_ipt(__file__)[0]
    return [int(x) for x in line.split(',')]


def run_intcode(ipt):
    for i in range(0, len(ipt), 4):
        opcode = ipt[i]
        if opcode not in [1, 2, 99]:
            # logging.info(f'error, unknown opcode {opcode} found at id {i}')
            return []
        elif opcode == 99:
            # logging.info(f'halted at index {i}');
            break
        val1 = ipt[ipt[i + 1]]
        val2 = ipt[ipt[i + 2]]
        ipt[ipt[i + 3]] = val1 + val2 if opcode == 1 else val1 * val2
    return ipt


def run_w_setup(ipt, noun, verb):
    ipt[1] = noun
    ipt[2] = verb
    return run_intcode(ipt)


def run():
    sol = []
    for n in range(100):
        for v in range(100):
            res = run_w_setup(reset_ipt(), n, v)
            if res != [] and res[0] == 19690720:
                sol.append(100 * n + v)
                logging.info(f'verb = {v}, noun = {n}, res = {100*n+v}')
    return sol[0]

# -*- coding: utf-8 -*-
import os


def read_ipt(current_file, formatter=lambda x: x):
    dir_name = os.path.dirname(os.path.abspath(current_file))
    input_file = os.path.join(dir_name, 'data/input.txt')

    with open(input_file, 'r') as file:
        return [formatter(x) for x in file]

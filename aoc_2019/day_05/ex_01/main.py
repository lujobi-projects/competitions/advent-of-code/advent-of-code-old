# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt

opcode_len = {'1': 4, '2': 4, '3': 2, '4': 2, '99': 0}


def run_intcode(code):
    ins_ptr = 0
    while True:
        print('\n')
        print(ins_ptr, code[225])
        ins = code[ins_ptr]
        modes = ins // 100
        opcode = ins % 100
        if str(opcode) not in opcode_len.keys():
            logging.info(f'error, unknown opcode {opcode} at instruction {ins} found at id {ins_ptr}')
            return []
        elif opcode == 99:
            logging.info(f'halted at index {ins_ptr}')
            break
        elif opcode in [1, 2]:
            if modes // 100 != 0:
                logging.info(f'error, unknown modes set in opcode {opcode} at instruction {ins} found at id {ins_ptr}')
                return []
            val1 = code[code[ins_ptr + 1]] if modes % 10 == 0 else code[ins_ptr + 1]
            val2 = code[code[ins_ptr + 2]] if modes // 10 == 0 else code[ins_ptr + 2]
            code[code[ins_ptr + 3]] = val1 + val2 if opcode == 1 else val1 * val2
        elif opcode == 3:
            code[code[ins_ptr + 1]] = input('insert \n')
        elif opcode == 4:
            print(code[code[ins_ptr + 1]])
        ins_ptr += opcode_len[str(opcode)]
    return code

    """for i in range(0, len(ipt), 4):
        opcode = ipt[i]
        if opcode not in [1, 2, 99]:
            #logging.info(f'error, unknown opcode {opcode} found at id {i}')
            return []
        elif opcode == 99:
            #logging.info(f'halted at index {i}');
            break
        val1 = ipt[ipt[i + 1]]
        val2 = ipt[ipt[i + 2]]
        ipt[ipt[i + 3]] = val1 + val2 if opcode == 1 else val1 * val2
    return ipt
    """


def run():
    line = read_ipt(__file__)[0]
    intcode = [int(x) for x in line.split(',')]

    run_intcode(intcode)
    return 0

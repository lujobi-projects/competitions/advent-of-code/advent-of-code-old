# -*- coding: utf-8 -*-
import importlib
import json
import logging
import os
import unittest

from ddt import ddt, file_data

TEST_FILE = 'test.json'

curr_dir = os.path.dirname(os.path.abspath(__file__))
days = list(map(lambda x: f'day_{x:02}', range(1, 26)))
logging.basicConfig(level=logging.WARN)


@ddt
class FooTestCase(unittest.TestCase):
    def test_json_correctness(self):

        with open(os.path.join(curr_dir, TEST_FILE)) as json_file:
            tests = json.load(json_file)
        for d in os.listdir():
            if d in days and os.path.isdir(os.path.join(curr_dir, d)):
                self.assertIn(d, map(lambda i: i['day'], tests), f'missing {d} in {TEST_FILE}')

    @file_data(os.path.join(curr_dir, TEST_FILE))
    def test_correct_output(self, day, sol):
        if sol:
            ex_01 = importlib.import_module(f'{day}.ex_01.main')
            ex_02 = importlib.import_module(f'{day}.ex_02.main')
            self.assertEqual(ex_01.run(), sol[0])
            self.assertEqual(ex_02.run(), sol[1])


def run():
    unittest.main()


if __name__ == '__main__':
    run()

# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def move(d, val, x_pos, y_pos):
    if d == 'N':
        y_pos += val
    elif d == 'S':
        y_pos -= val
    elif d == 'E':
        x_pos += val
    elif d == 'W':
        x_pos -= val
    return x_pos, y_pos


def rotate(x, y, ct):
    ct = (4 + ct) % 4 if ct < 0 else ct
    for _ in range(ct):
        x_new = y
        y = -x
        x = x_new
    return x, y


def run():
    ipt = read_ipt(__file__)
    # 0 = nord, 1 = east, 2 = south, 3 = west
    dirs = ['N', 'E', 'S', 'W']

    x_pos = 0
    y_pos = 0

    x_wp = 10
    y_wp = 1

    for i in ipt:
        d = i[0]
        val = int(i[1:])

        if d in dirs:
            x_wp, y_wp = move(d, val, x_wp, y_wp)
        elif d == 'R':
            x_wp, y_wp = rotate(x_wp, y_wp, int(val / 90))
        elif d == 'L':
            x_wp, y_wp = rotate(x_wp, y_wp, -int(val / 90))
        elif d == 'F':
            x_pos += x_wp * val
            y_pos += y_wp * val

        # print(x_pos, y_pos, x_wp, y_wp)

    return abs(x_pos) + abs(y_pos)

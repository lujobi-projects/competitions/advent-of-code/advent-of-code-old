# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def move(d, val, x_pos, y_pos):
    if d == 'N':
        y_pos -= val
    elif d == 'S':
        y_pos += val
    elif d == 'E':
        x_pos -= val
    elif d == 'W':
        x_pos += val
    return x_pos, y_pos


def run():
    ipt = read_ipt(__file__)
    # 0 = nord, 1 = east, 2 = south, 3 = west
    dirs = ['N', 'E', 'S', 'W']

    curr_dir = 1
    x_pos = 0
    y_pos = 0

    for i in ipt:
        d = i[0]
        val = int(i[1:])

        if d in dirs:
            x_pos, y_pos = move(d, val, x_pos, y_pos)
        elif d == 'R':
            curr_dir = (curr_dir + int(val / 90)) % 4
        elif d == 'L':
            curr_dir = (4 + curr_dir - int(val / 90)) % 4
        elif d == 'F':
            x_pos, y_pos = move(dirs[curr_dir], val, x_pos, y_pos)

    return abs(x_pos) + abs(y_pos)

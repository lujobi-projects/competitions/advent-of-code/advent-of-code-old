# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__, int)
    for i in ipt:
        for j in ipt:
            if i + j == 2020:
                logging.info(f'{i} + {j} = 2020; {i} * {j} = {i*j}')
                return i * j
    return 0

# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__, int)
    for i in ipt:
        for j in ipt:
            for k in ipt:
                if i + j + k == 2020:
                    logging.info(f'{i} + {j} + {k}= 2020; {i} * {j} * {k} = {i*j*k}')
                    return i * j * k
    return 0

# -*- coding: utf-8 -*-
import logging

import numpy as np
from scipy import signal
from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)

    game = np.zeros((1, 1, len(ipt[0]) - 1, len(ipt)), dtype=int)

    for i, line in enumerate(ipt):
        for j, cube in enumerate(line.replace('\n', '')):
            game[0][0][i][j] = 1 if cube == '#' else 0

    k = np.array(
        [
            [[[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[1, 1, 1], [1, 1, 1], [1, 1, 1]]],
            [[[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[1, 1, 1], [1, 0, 1], [1, 1, 1]], [[1, 1, 1], [1, 1, 1], [1, 1, 1]]],
            [[[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[1, 1, 1], [1, 1, 1], [1, 1, 1]]],
        ]
    )
    neighbor_ct = []
    for _ in range(6):
        game_padded = np.pad(game.copy(), [(1, 1), (1, 1), (1, 1), (1, 1)])
        neighbor_ct = signal.convolve(game_padded, k, mode='same')
        res = np.zeros_like(neighbor_ct)

        res[neighbor_ct == 3] = 1
        res[np.logical_and(neighbor_ct == 2, game_padded == 1)] = 1
        game = res

    return np.sum(game)

# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__, int)

    ipt.append(0)
    ipt.append(max(ipt) + 3)
    list.sort(ipt)

    m = max(ipt)
    l = len(ipt)
    cache = [None] * l

    def rec_count(i):
        ct = 0
        jolt = ipt[i]

        for offset in range(min(l - i - 1, 3)):
            new_item = ipt[i + offset + 1]
            if new_item == m and new_item - jolt <= 3:
                ct += 1
            elif new_item - jolt <= 3:
                ct += cache[i + offset + 1] if cache[i + offset + 1] else rec_count(i + offset + 1)
            else:
                break
        cache[i] = ct
        return ct

    return rec_count(0)

# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__, int)
    list.sort(ipt)

    diffs = [0, 0, 0]

    for i in range(1, len(ipt)):
        diffs[ipt[i] - ipt[i - 1] - 1] += 1

    return (diffs[0] + 1) * (diffs[2] + 1)

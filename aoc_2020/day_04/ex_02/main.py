# -*- coding: utf-8 -*-
import logging
import re

from utils.read_ipt import read_ipt

req_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']


def check_field(field):
    key = field.split(':')[0]
    value = field.split(':')[1]
    if key == 'cid':
        return True
    elif key == 'byr':
        return 1920 <= int(value) <= 2002
    elif key == 'iyr':
        return 2010 <= int(value) <= 2020
    elif key == 'eyr':
        return 2020 <= int(value) <= 2030
    elif key == 'hgt':
        if 'cm' in value:
            length = int(value[:-2])
            return 150 <= length <= 193
        elif 'in' in value:
            length = int(value[:-2])
            return 59 <= length <= 76
    elif key == 'hcl':
        r = r'#[0-9a-f]{6}'
        return re.split(r, value) == ['', '']
    elif key == 'ecl':
        return value in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']
    elif key == 'pid':
        r = r'[0-9]{9}'
        return re.split(r, value) == ['', '']
    return False


def check_passport(passport):
    passport = passport.replace('\n', ' ')
    if not all([x in passport for x in req_fields]):
        return False
    return all(check_field(x) for x in passport.split(' ')[:-1])


def run():
    ipt = read_ipt(__file__)
    total = 0
    nop = 0
    passport = ''
    for line in ipt:
        if line == '\n':
            nop += 1
            if check_passport(passport):
                total += 1
            passport = ''
        else:
            passport += line
    logging.info(f'found total of {nop} passports')
    return total

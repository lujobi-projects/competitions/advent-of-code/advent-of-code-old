# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt

req_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']


def run():
    ipt = read_ipt(__file__)
    total = 0
    nop = 0
    passport = ''
    for line in ipt:
        if line == '\n':
            nop += 1
            if all([x in passport for x in req_fields]):
                total += 1
            else:
                tmp = passport.replace('\n', ' ')
            passport = ''
        else:
            passport += line
    logging.info(f'found total of {nop} passports')
    return total

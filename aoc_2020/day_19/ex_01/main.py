# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def checkrule(no, st, rules):
    r = rules[no]
    if st == '':
        return False, st
    elif len(r[0][0]) == 1 and ord('a') <= ord(r[0][0]) <= ord('z'):
        return st[0] == r[0][0], st[1:]
    else:
        orig = st
        for rule in r:
            rff = True
            st = orig
            for subr in rule:
                res, st = checkrule(int(subr), st, rules)
                rff = rff and res
                if not rff:
                    break
            if rff:
                return True, st
    return False, st


def run():
    ipt = read_ipt(__file__)

    rules = [None] * 200
    messages = []

    for line in ipt:
        if ':' in line:
            splitted = line.split(':')
            rule = splitted[1][1:].replace('"', '').split('| ')
            rules[int(splitted[0])] = list(map(lambda a: a[:-1].split(' '), rule))
        elif line != '\n':
            messages.append(line.replace('\n', ''))

    ct = 0

    for m in messages:
        res, st = checkrule(0, m, rules)
        if res and st == '':
            ct = ct + 1

    return ct

# -*- coding: utf-8 -*-
from functools import reduce

from utils.read_ipt import read_ipt


def chinese_remainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a * b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod, prod


def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1:
        return 1
    while a > 1:
        q = a // b
        a, b = b, a % b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0:
        x1 += b0
    return x1


def run():
    ipt = read_ipt(__file__)

    ids = []
    diffs = []
    diff = 0

    for i in ipt[1].split(','):
        if i != 'x':
            ids.append(int(i))
            diffs.append(diff)
            diff += 1
        else:
            diff += 1
    diffs[0] = 0

    res, prod = chinese_remainder(ids, diffs)

    temp = prod - res

    return temp

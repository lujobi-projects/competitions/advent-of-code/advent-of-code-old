# -*- coding: utf-8 -*-
import logging
import math

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)

    time = int(ipt[0])
    ids = []

    for i in ipt[1].split(','):
        if i != 'x':
            ids.append(int(i))

    m_time, min_id = -1, -1

    for i, id in enumerate(ids):
        diff = math.ceil(time / id) * id - time
        if min_id == -1 or diff < m_time:
            m_time, min_id = diff, i

    return ids[min_id] * m_time

# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def count_rec(bag_map, search_bag):
    ct = 1
    for key, count in bag_map[search_bag].items():
        ct += count * count_rec(bag_map, key)
    return ct


def run():
    ipt = read_ipt(__file__)
    bag_map = {}
    for i in ipt:
        i = i.replace('.', '').replace('\n', '')
        splitted = i.split(' contain ')

        bag_map[splitted[0][:-1]] = {}
        if 'no other bags' not in splitted[1]:
            for s in splitted[1].split(', '):
                s_sub = s.split(' ', 1)
                cleaned = s_sub[1]
                if cleaned[-1] == 's':
                    cleaned = cleaned[:-1]
                bag_map[splitted[0][:-1]][cleaned] = int(s_sub[0])

    return count_rec(bag_map, 'shiny gold bag') - 1

# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def check_rec(bag_map, search_bag, to_check):
    ct = 0
    for b in to_check:
        items = bag_map[b]
        if search_bag in items or check_rec(bag_map, search_bag, items) > 0:
            ct += 1
    return ct


def run():
    ipt = read_ipt(__file__)
    bag_map = {}
    for i in ipt:
        i = i.replace('.', '').replace('\n', '')
        splitted = i.split(' contain ')
        containing = []

        if 'no other bags' not in splitted[1]:
            for s in splitted[1].split(', '):
                cleaned = s.split(' ', 1)[1]
                if cleaned[-1] == 's':
                    cleaned = cleaned[:-1]
                containing.append(cleaned)
        bag_map[splitted[0][:-1]] = containing

    return check_rec(bag_map, 'shiny gold bag', bag_map.keys())

# -*- coding: utf-8 -*-
import logging

import numpy as np
from utils.read_ipt import read_ipt


def run_ipt(ipt):
    visited = np.zeros_like(ipt, dtype=int)
    acc = 0
    ins_ptr = 0

    while ins_ptr < len(visited) and visited[ins_ptr] != 1:
        visited[ins_ptr] += 1
        line = ipt[ins_ptr]
        if 'acc' in line:
            acc += int(line.split(' ')[1])
        if 'jmp ' in line:
            ins_ptr += int(line.split(' ')[1])
        else:
            ins_ptr += 1
    return acc, len(visited) == ins_ptr


def run():
    ipt = read_ipt(__file__, lambda x: x.replace('\n', ''))
    acc = 0
    for i, ins in enumerate(ipt):
        if 'jmp' in ins:
            ipt[i] = f'nop {ins[4:]}'
        elif 'nop' in ins:
            ipt[i] = f'jmp {ins[4:]}'

        acc, term = run_ipt(ipt)
        ipt[i] = ins
        if term:
            break
    return acc

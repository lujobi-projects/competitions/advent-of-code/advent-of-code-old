# -*- coding: utf-8 -*-
import logging

import numpy as np
from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    visited = np.zeros_like(ipt, dtype=int)
    acc = 0
    ins_ptr = 0

    while visited[ins_ptr] != 1:
        visited[ins_ptr] += 1
        line = ipt[ins_ptr]
        if 'acc' in line:
            acc += int(line.split(' ')[1])
        if 'jmp ' in line:
            ins_ptr += int(line.split(' ')[1])
        else:
            ins_ptr += 1

    return acc

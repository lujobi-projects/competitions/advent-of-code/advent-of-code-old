# -*- coding: utf-8 -*-
import logging

import numpy as np
from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    ct = 0
    ans = np.zeros(26)
    for line in ipt:
        if line == '\n':
            ct += np.sum(ans)
            ans = np.zeros(26)
        else:
            for c in line.replace('\n', ''):
                ans[ord(c) - ord('a')] = 1

    return int(ct + np.sum(ans))

# -*- coding: utf-8 -*-
import logging

import numpy as np
from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    ct = 0
    ans = np.zeros(26, dtype=int)
    gr_ct = 0
    for line in ipt:
        if line == '\n':
            ct += len(np.where(ans == gr_ct)[0])
            ans = np.zeros(26)
            gr_ct = 0
        else:
            for c in line.replace('\n', ''):
                ans[ord(c) - ord('a')] += 1
            gr_ct += 1

    return int(ct + len(np.where(ans == gr_ct)[0]))

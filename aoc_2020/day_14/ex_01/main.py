# -*- coding: utf-8 -*-
import logging

import numpy as np
from utils.read_ipt import read_ipt


def process_mask(mask):
    mask_1 = 0
    mask_0 = 0
    for i, no in enumerate(mask[::-1]):
        if no == '1':
            mask_1 += 2 ** i
        elif no == '0':
            mask_0 += 2 ** i
    return mask_0, mask_1


def run():
    ipt = read_ipt(__file__)
    mem = np.zeros(100000)

    mask_1 = 0
    mask_0 = 0

    for i in ipt:
        if 'mask' in i:
            mask_0, mask_1 = process_mask(i.replace('mask = ', '').replace('\n', ''))
        else:
            i = i.replace('mem[', '').replace('\n', '')
            i = i.split('] = ')
            no = int(i[1])
            # print(int(i[0]), f'{no:b}', no)
            no = no & (~mask_0)
            no = no | mask_1
            mem[int(i[0])] = no

    return sum(mem)

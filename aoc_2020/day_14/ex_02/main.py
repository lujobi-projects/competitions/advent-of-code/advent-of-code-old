# -*- coding: utf-8 -*-
import logging
from functools import reduce

import numpy as np
from utils.read_ipt import read_ipt


def process_mask(mask):
    mask_1 = [0]
    mask_0 = [0]

    def extend_mask(acc, m, i):
        acc.append(m)
        m1 = m + 2 ** i
        acc.append(m1)
        return acc

    for i, no in enumerate(mask[::-1]):
        if no == '1':
            mask_1 = list(map(lambda x: x + 2 ** i, mask_1))
        elif no == '0':
            mask_0 = list(map(lambda x: x + 2 ** i, mask_0))
        elif no == 'X':
            mask_0 = reduce(lambda acc, elem: extend_mask(acc, elem, i), mask_0, [])
            mask_1 = reduce(lambda acc, elem: extend_mask(acc, elem, i), mask_1, [])
    return mask_0, mask_1


def run():
    ipt = read_ipt(__file__)
    mem = {}

    mask_1 = 0
    mask_0 = 0

    for i in ipt:
        if 'mask' in i:
            mask_0, mask_1 = process_mask(i.replace('mask = ', '').replace('\n', ''))
        else:
            i = i.replace('mem[', '').replace('\n', '')
            i = i.split('] = ')
            no = int(i[1])
            mem_addr = int(i[0])
            # print(int(i[0]), f'{no:b}', no)
            for m0, m1 in zip(mask_0, mask_1):
                mem_addr = mem_addr & (m0)
                mem_addr = mem_addr | m1
                mem[str(mem_addr)] = no

    return sum(mem.values())

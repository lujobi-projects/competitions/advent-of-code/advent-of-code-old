# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    ids = [0] * len(ipt)
    for idx, line in enumerate(ipt):
        row = 0
        col = 0
        for i, r in enumerate(line[0:7]):
            row += 2 ** (6 - i) if r == 'B' else 0
        for i, r in enumerate(line[7:10]):
            col += 2 ** (2 - i) if r == 'R' else 0
        ids[idx] = row * 8 + col

    return max(ids)

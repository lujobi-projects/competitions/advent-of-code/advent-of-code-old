# -*- coding: utf-8 -*-
import logging

import numpy as np
from utils.read_ipt import read_ipt

NUM_ROWS = 128
NUM_COLS = 8


def run():
    ipt = read_ipt(__file__)

    seats = np.zeros([NUM_ROWS, NUM_COLS])
    for idx, line in enumerate(ipt):
        row = 0
        col = 0
        for i, r in enumerate(line[0:7]):
            row += 2 ** (6 - i) if r == 'B' else 0
        for i, r in enumerate(line[7:10]):
            col += 2 ** (2 - i) if r == 'R' else 0
        seats[row, col] = 1

    minr = 0
    for i, r in enumerate(seats):
        if 0 not in r:
            minr = i
            break
    maxr = 0
    for i, r in reversed(list(enumerate(seats))):
        if 0 not in r:
            maxr = i
            break
    r, c = np.where(seats[minr:maxr] == 0)
    return (r[0] + minr) * NUM_COLS + c[0]

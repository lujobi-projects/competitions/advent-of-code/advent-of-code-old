# -*- coding: utf-8 -*-
import logging

import numpy as np
from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)

    number_set = set()
    tickets = []

    near = False
    your = False
    your_ticket = []

    ruledict = {}

    for line in ipt:
        if 'or' in line:
            info = line.split(': ')
            nos = info[1].replace('\n', '').split(' or ')
            poss_nos = []
            for ran in nos:
                n = ran.split('-')
                for i in range(int(n[0]), int(n[1]) + 1):
                    poss_nos.append(i)
            number_set.update(poss_nos)
            ruledict[info[0]] = poss_nos
        elif 'nearby' in line:
            near = True
        elif 'your' in line:
            your = True
        elif your:
            your_ticket = [int(i) for i in line.split(',')]
            your = False
        elif near:
            tickets.append([int(i) for i in line.split(',')])

    err = 0
    valid = np.ones(len(tickets), dtype=bool)

    for i, t in enumerate(tickets):
        for no in t:
            if no not in number_set:
                valid[i] = False

    tickets = np.array(tickets)[np.where(valid)[0]]
    rule = []

    for k, ran in ruledict.items():
        ind = []
        for val in range(len(tickets[0])):
            valid = True
            for t in tickets:
                if t[val] not in ran:
                    valid = False
                    break
            if valid:
                ind.append(val)
        rule.append(ind)

    print(rule)

    sorted_list = [None] * len(rule)
    rule_orig = rule.copy()
    ct = 1

    while None in sorted_list:
        for i, r in enumerate(rule):
            if len(r) == ct:
                for n in r:
                    if n not in sorted_list:
                        sorted_list[i] = n
                        # rule.remove(r)
                        break
        ct += 1

    print(rule)
    print(sorted_list)

    sorted_list_named = []
    for r in sorted_list:
        sorted_list_named.append(list(ruledict.keys())[r])

    print(sorted_list_named)

    sol = 1
    for i, name in enumerate(sorted_list_named):
        if 'departure' in name:
            r = your_ticket[i]
            print(r)
            print(name)
            sol *= your_ticket[i]

    return sol

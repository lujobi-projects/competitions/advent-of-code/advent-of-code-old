# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)

    number_set = set()
    tickets = []

    near = False
    your = False
    your_ticket = []

    for line in ipt:
        if 'or' in line:
            nos = line.split(': ')[1].replace('\n', '').split(' or ')
            for ran in nos:
                n = ran.split('-')
                for i in range(int(n[0]), int(n[1]) + 1):
                    number_set.add(i)
        elif 'nearby' in line:
            near = True
        elif 'your' in line:
            your = True
        elif your:
            your_ticket = [int(i) for i in line.split(',')]
            your = False
        elif near:
            tickets.append([int(i) for i in line.split(',')])

    err = 0

    for t in tickets:
        for no in t:
            if no not in number_set:
                err += no
    return err

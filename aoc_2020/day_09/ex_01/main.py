# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt

preamble_len = 25


def test_add(nolist, sum):
    for i in nolist:
        for j in nolist:
            if j != i and i + j == sum:
                return True
    return False


def run():
    ipt = read_ipt(__file__, int)

    for i in range(len(ipt) - preamble_len - 1):
        if not test_add(ipt[i: i + preamble_len], ipt[i + preamble_len]):
            return ipt[i + preamble_len]

    return 0

# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt

preamble_len = 25

search_no = 21806024


def find_cont(nolist, req):
    for i in range(len(nolist)):
        for j in range(1, len(nolist)):
            if sum(nolist[i: i + j]) == req:
                return min(nolist[i: i + j]) + max(nolist[i: i + j])


def run():
    ipt = read_ipt(__file__, int)

    return find_cont(ipt, search_no)

# -*- coding: utf-8 -*-
import logging
import re

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    wrong_pwd = 0
    for i in ipt:
        splitted = i[:-1].split(' ')
        min_ct = int(splitted[0].split('-')[0])
        max_ct = int(splitted[0].split('-')[1])
        char = splitted[1][:-1]
        if not (splitted[2][min_ct - 1] == char) != (splitted[2][max_ct - 1] == char):
            wrong_pwd += 1

    logging.info(f'Of a total of {len(ipt)} passwords, {wrong_pwd} are wrong, leaving {len(ipt)-wrong_pwd} correct')
    return len(ipt) - wrong_pwd

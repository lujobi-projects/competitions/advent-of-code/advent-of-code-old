# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def check(foods, guesses):
    for f in foods:
        in_alergs = set([None])
        for i in f['in']:
            in_alergs.add(guesses[i])
        if not set(f['al']).issubset(in_alergs):
            return False
    return True


def tryout(ingrs, guesses, alergs, foods, first=False):
    sol = set()
    if len(alergs) == 0 and check(foods, guesses):
        for i in ingrs:
            if guesses[i] == None:
                sol.add(i)
        return sol

    for a in alergs:
        if first:
            print(a)
        a_temp = alergs.copy()
        a_temp.remove(a)
        for i in ingrs:
            i_temp = ingrs.copy()
            i_temp.remove(i)
            g_temp = guesses.copy()
            g_temp[i] = a
            temp_sol = tryout(i_temp, g_temp, a_temp, foods)
            if len(sol) == 0:
                sol = temp_sol
            elif len(temp_sol) > 0:
                sol.intersection_update(temp_sol)
    return sol


def run():
    ipt = read_ipt(__file__)

    foods = []
    ingrs = set()
    alergs = set()
    guesses = {}
    for line in ipt:
        line = line.replace(')', '').replace('\n', '')
        splitted = line.split(' (contains ')
        ingr = splitted[0].split(' ')
        alergen = splitted[1].split(', ')
        foods.append({'in': ingr, 'al': alergen})
        ingrs = ingrs.union(ingr)
        alergs = alergs.union(alergen)
        for i in ingr:
            if i not in guesses.keys():
                guesses[i] = None

    sol = tryout(ingrs, guesses, alergs, foods, True)

    ct = 0
    for s in sol:
        for f in foods:
            if s in f['in']:
                ct += 1
    print(sol)
    return ct

# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt

END_NO = 2020


def run():
    inputs = [20, 9, 11, 0, 1, 2]

    last_occurred = {}
    last = inputs[-1]

    for i in range(1, END_NO + 1):
        last_seen = 0
        new_last = 0
        if i - 1 > len(inputs):
            last_seen = last_occurred.get(str(last), 0)
            if last_seen != 0:
                new_last = i - last_seen - 1
            else:
                new_last = 0
        if i != 1:
            last_occurred[str(last)] = i - 1
        if i <= len(inputs):
            last = inputs[i - 1]
        else:
            last = new_last

    return last

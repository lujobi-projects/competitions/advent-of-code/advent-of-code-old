# -*- coding: utf-8 -*-
import logging
from functools import reduce

from utils.read_ipt import read_ipt


def op(o, a, b):
    return a + b if o == '+' else a * b


def process_ex(e):
    if e[0] == '(':
        res, e = eval_ex(e[1:])
    else:
        res = int(e[0])
    e = e[1:]
    return res, e


def eval_ex(e):
    lhs, e = process_ex(e)
    while e and e[0] != '\n' and e[0] != ')':
        o = e[0]
        e = e[1:]
        rhs, e = process_ex(e)
        lhs = op(o, lhs, rhs)
    return lhs, e


def process_line(acc, line):
    res, _ = eval_ex(line.replace(' ', ''))
    return acc + res


def run():
    ipt = read_ipt(__file__)
    return reduce(process_line, ipt, 0)

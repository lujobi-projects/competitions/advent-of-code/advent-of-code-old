# -*- coding: utf-8 -*-
import logging

import numpy as np
from scipy import signal
from utils.read_ipt import read_ipt

PUZZLE_SIZE = 3


def rotate(edges):
    res = [edges[-1]]
    res.extend(edges[:-1])
    return res


def flip(edges):
    res = edges.copy()
    res[0] = res[2]
    res[2] = edges[0]
    return res


mask = [[0, 1, 0], [1, 0, 1], [0, 1, 0]]


def calcNeighbors(setup):
    occupied = np.zeros_like(setup)
    occupied[setup > 0] = 1
    neighbor_ct = signal.convolve(occupied, mask, mode='same')
    return np.where(np.logical_and(neighbor_ct > 0, neighbor_ct < 4, occupied != 1))


def checkfit(setup, tiles, x, y):

    curr_tile = tiles[str(setup[y, x])]
    if x > 0 and setup[y, x - 1] != 0 and tiles[str(setup[y, x - 1])][1] != curr_tile[3]:
        return False
    elif x < 2 * PUZZLE_SIZE - 2 and setup[y, x + 1] != 0 and tiles[str(setup[y, x + 1])][3] != curr_tile[1]:
        return False
    elif y > 0 and setup[y - 1, x] != 0 and tiles[str(setup[y - 1, x])][2] != curr_tile[0]:
        return False
    elif x < 2 * PUZZLE_SIZE - 2 and setup[y + 1, x] != 0 and tiles[str(setup[y + 1, x])][0] != curr_tile[2]:
        return False
    return True


def assemble(setup, tiles, open_keys):
    y_neigh, x_neigh = calcNeighbors(setup)
    print(setup)
    print(open_keys)
    # input()

    if open_keys == []:
        return setup, True

    for x, y in zip(x_neigh, y_neigh):
        for k in open_keys:
            success = False
            for _ in range(2):
                for _ in range(4):
                    orig = setup.copy()
                    setup[y, x] = k
                    fit = checkfit(setup, tiles, x, y)
                    if fit:
                        newkeys = open_keys.copy()
                        newkeys.remove(k)
                        print('djhjdhjdhdjhd', newkeys, k)
                        sol, success = assemble(setup, tiles, newkeys)
                        if success:
                            return sol, True
                        else:
                            tmp = [k]
                            tmp.extend(open_keys)
                            open_keys = tmp
                    else:
                        setup = orig.copy()
                    tiles[str(k)] = rotate(tiles[str(k)])
                tiles[str(k)] = flip(tiles[str(k)])
    return setup, False


PS = 3
TS = 10


def assemble2(setup, tiles, open_keys):
    y_neigh, x_neigh = calcNeighbors(setup)
    print(setup)
    print(open_keys)

    for x in range(PS):
        for y in range(PS):
            # input()


def run():
    ipt = read_ipt(__file__)

    tiles = {}
    current_setup = np.zeros((2 * PUZZLE_SIZE - 1, 2 * PUZZLE_SIZE - 1), dtype=int)

    tile = 0
    tilearr = []
    for line in ipt:
        if 'Tile' in line:
            tile = int(line[5:9])
        elif line == '\n':
            tiles[str(tile)] = tilearr
            tilearr = []
        else:
            tilearr.append(list(map(lambda e: 1 if e == '#' else 0, line.replace('\n', ''))))

    tiles[str(tile)] = tilearr
    tile_edges = {}
    keys = []
    for k, t in tiles.items():
        edges = [t[0]]
        keys.append(int(k))
        edges.append(list(map(lambda l: l[-1], t)))
        edges.append(t[-1])
        edges.append(list(map(lambda l: l[0], t)))
        tile_edges[k] = edges
    current_setup[PUZZLE_SIZE - 1][PUZZLE_SIZE - 1] = keys[0]
    keys = keys[1:]

    s, ss = assemble(current_setup, tile_edges, keys)
    print(s)

    return 0

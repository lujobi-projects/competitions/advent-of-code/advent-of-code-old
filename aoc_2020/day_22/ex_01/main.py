# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    ct = -1
    res = [[], []]
    for line in ipt:
        if 'Player' in line:
            ct += 1
        elif line != '\n':
            res[ct].append(int(line.replace('\n', '')))
    ct = 0

    while len(res[0]) != 0 and len(res[1]) != 0:
        ct += 1
        print(ct)
        p1 = res[0][0]
        p2 = res[1][0]
        res[0] = res[0][1:]
        res[1] = res[1][1:]
        if p1 > p2:
            res[0].append(p1)
            res[0].append(p2)
        else:
            res[1].append(p2)
            res[1].append(p1)

    hand = res[1] if len(res[0]) == 0 else res[0]

    ct = len(hand)
    sum_res = 0
    for h in hand:
        sum_res += ct * h
        ct -= 1

    print(res)
    return sum_res

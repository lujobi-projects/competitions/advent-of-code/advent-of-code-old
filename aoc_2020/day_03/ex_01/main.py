# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)

    total = 0
    for i, line in enumerate(ipt):
        line = line.replace('\n', '')
        if line[(i * 3) % len(line)] == '#':
            total += 1
    return total

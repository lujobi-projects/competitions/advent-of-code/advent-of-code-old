# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)

    tot_mul = 1
    policies = [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]

    for p in policies:
        total = 0
        for i, line in enumerate(ipt[:: p[1]]):
            line = line.replace('\n', '')
            if line[(i * p[0]) % len(line)] == '#':
                total += 1
        logging.info(f'policy: {p}: {total}')
        tot_mul *= total

    logging.info(f'multiplied: {tot_mul}')
    return tot_mul

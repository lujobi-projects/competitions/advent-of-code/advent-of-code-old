# -*- coding: utf-8 -*-
import logging

import numpy as np
from utils.read_ipt import read_ipt


def ct_dir(x, y, x_dir, y_dir, arr, p):
    i_len, j_len = np.shape(arr)
    for i in range(1, max(np.shape(arr)) + 1):
        x_new = x + x_dir * i
        y_new = y + y_dir * i
        if x_new >= i_len or y_new >= j_len or x_new == 0 or y_new == 0 or arr[x_new, y_new] == 1:
            return 0
        elif arr[x_new, y_new] == 8:
            if p:
                print(x_new, y_new)
            return 1
    return 0


def occupy(seats, p):
    res = np.copy(seats)
    for i, ln in enumerate(seats):
        for j, elem in enumerate(ln):
            if elem != 0:
                full_ct = 0

                full_ct += ct_dir(i, j, 1, 0, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, -1, 0, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, 0, 1, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, 0, -1, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, 1, 1, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, 1, -1, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, -1, 1, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, -1, -1, seats, p)

                if p:
                    print(full_ct)
                if p:
                    input()

                if full_ct == 0:
                    res[i, j] = 8
                if full_ct >= 5:
                    res[i, j] = 1
    return res


def print_arr(a):
    r = ''
    for l in a:
        for e in l:
            if e == 8:
                r += '#'
            elif e == 1:
                r += 'L'
            else:
                r += '.'
        r += '\n'
    print(r)


def run():
    ipt = read_ipt(__file__)
    test = [([1 if x == 'L' else 0 for x in l.replace('\n', '')]) for l in ipt]

    arr = np.array(test, dtype=int)
    ct = 0
    while True:
        arr_new = occupy(np.copy(arr), False)
        if np.alltrue(arr == arr_new):
            break
        arr = arr_new
        # ct += 1

    return np.count_nonzero(arr == 8) - 1


"""
cleaner but not working:

import logging
import numpy as np
from utils.read_ipt import read_ipt


def ct_dir(x, y, x_dir, y_dir, arr, p):
    i_len, j_len = np.shape(arr)
    for i in range(1, max(np.shape(arr)) + 1):
        x_new = x + x_dir * i
        y_new = y + y_dir * i
        if x_new >= i_len or y_new >= j_len or x_new == 0 or y_new == 0 or arr[
                x_new, y_new] == 1:
            return 0
        elif arr[x_new, y_new] == 8:
            if p:
                print(x_new, y_new)
            return 1
    return 0


def occupy(seats, p):
    res = np.copy(seats)
    for i, ln in enumerate(seats):
        for j, elem in enumerate(ln):
            if elem != 0:
                full_ct = 0

                full_ct += ct_dir(i, j, 1, 0, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, -1, 0, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, 0, 1, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, 0, -1, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, 1, 1, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, 1, -1, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, -1, 1, seats, p)
                if p:
                    print(full_ct)
                full_ct += ct_dir(i, j, -1, -1, seats, p)

                if p:
                    print(full_ct)
                if p:
                    input()

                if full_ct == 0:
                    res[i, j] = 8
                if full_ct >= 5:
                    res[i, j] = 1
    return res


def print_arr(a):
    r = ''
    for l in a:
        for e in l:
            if e == 8:
                r += '#'
            elif e == 1:
                r += 'L'
            else:
                r += '.'
        r += '\n'
    print(r)


def run():
    ipt = read_ipt(__file__)
    test = [([1 if x == 'L' else 0 for x in l.replace('\n', '')]) for l in ipt]

    arr = np.array(test, dtype=int)
    ct = 0
    while True:
        print(ct)
        arr_new = occupy(np.copy(arr), ct == 1)
        print_arr(arr_new)
        if np.alltrue(arr == arr_new):
            break
        arr = arr_new
        #ct += 1

    return np.count_nonzero(arr == 8)
"""

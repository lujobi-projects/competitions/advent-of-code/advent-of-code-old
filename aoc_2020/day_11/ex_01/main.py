# -*- coding: utf-8 -*-
import logging

import numpy as np
from numpy.core.defchararray import mod
from scipy import signal
from utils.read_ipt import read_ipt

mask = [[1, 1, 1], [1, 0, 1], [1, 1, 1]]


def occupy(seats):
    s_con = signal.convolve(seats, np.array(mask), mode='same')
    seats[np.logical_and(s_con <= 8, seats != 0)] = 8
    seats[np.logical_and(s_con >= 32, seats != 0)] = 1
    return seats


def run():
    ipt = read_ipt(__file__)
    test = [([1 if x == 'L' else 0 for x in l.replace('\n', '')]) for l in ipt]

    arr = np.array(test, dtype=int)

    while True:
        arr_new = occupy(np.copy(arr))
        if np.alltrue(arr == arr_new):
            break
        arr = arr_new

    return np.count_nonzero(arr == 8)


if ! [ "$1" -eq "$1" ] 2> /dev/null
then
    echo "argument 1 must be a number"
    return
fi

dir="day_$(printf %02d $1)"

mkdir $dir
mkdir "$dir/ex_01"
mkdir "$dir/ex_01/data"
touch "$dir/ex_01/data/input.txt"
cp ../template.py "$dir/ex_01/main.py"

mkdir "$dir/ex_02"
mkdir "$dir/ex_02/data"
ln "$dir/ex_01/data/input.txt" "$dir/ex_02/data/input.txt"
cp ../template.py "$dir/ex_02/main.py"

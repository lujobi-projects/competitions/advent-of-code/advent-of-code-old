# -*- coding: utf-8 -*-
import argparse
import importlib
import logging


def run():
    parser = argparse.ArgumentParser(description='Run AOC Files')
    days = list(map(lambda x: f'day_{x:02}', range(1, 26)))
    parser.add_argument('day', choices=days, default='')
    parser.add_argument('ex', choices=['ex_01', 'ex_02'], default='')

    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)

    print(f'Running Day {args.day}, Exercise {args.ex}')
    ex = importlib.import_module(f'{args.day}.{args.ex}.main')
    print(f'Result = {ex.run()}')


if __name__ == '__main__':
    run()

# -*- coding: utf-8 -*-
import logging

import numpy as np
from utils.read_ipt import read_ipt


def run():
    ipt = np.array(read_ipt(__file__, int))
    return np.count_nonzero(ipt[1:] - ipt[:-1] > 0)

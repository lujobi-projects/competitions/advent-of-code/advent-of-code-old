# -*- coding: utf-8 -*-
import logging

import numpy as np
from scipy import ndimage
from utils.read_ipt import read_ipt


def run():
    ipt = np.array(read_ipt(__file__, int))
    avg = np.convolve(ipt, np.ones(3))[2:-2]
    return np.count_nonzero(avg[1:] - avg[:-1] > 0)

# -*- coding: utf-8 -*-
import logging

import numpy as np
from utils.read_ipt import read_ipt


def run():
    oxy = np.array(read_ipt(__file__, lambda a: int(a, 2)))
    ipt_str = read_ipt(__file__)
    co2 = oxy.copy()
    cols = len(ipt_str[0]) - 1
    total = 0

    factor = 2 ** (cols - 1)
    while len(oxy) > 1 and factor >= 1:
        ones_ct = np.count_nonzero(oxy - total >= factor)
        if ones_ct >= len(oxy) / 2:
            oxy = oxy[np.where(oxy - total >= factor)]  # take ones
            total += factor
        else:
            oxy = oxy[np.where(oxy - total < factor)]  # take zeros
        factor /= 2

    total = 0
    factor = 2 ** (cols - 1)

    while len(co2) > 1 and factor >= 1:
        ones_ct = np.count_nonzero(co2 - total >= factor)
        if ones_ct >= len(co2) / 2:
            co2 = co2[np.where(co2 - total < factor)]  # take zeros
        else:
            co2 = co2[np.where(co2 - total >= factor)]  # take ones
            total += factor
        factor /= 2

    return oxy[0] * co2[0]

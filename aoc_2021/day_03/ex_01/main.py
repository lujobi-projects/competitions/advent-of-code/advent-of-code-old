# -*- coding: utf-8 -*-
import logging

import numpy as np
from utils.read_ipt import read_ipt


def run():

    ipt = np.array(read_ipt(__file__, lambda a: int(a, 2)), int)
    ipt_str = read_ipt(__file__)
    rows = len(ipt)
    cols = len(ipt_str[0]) - 1
    beta = 0
    gama = 0

    factor = 2 ** (cols - 1)
    while factor >= 1:
        ones_ct = np.count_nonzero(ipt >= factor)
        if ones_ct <= rows / 2:
            beta += factor
        else:
            gama += factor
        ipt %= factor
        factor //= 2

    return int(gama * beta)

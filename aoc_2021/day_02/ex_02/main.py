# -*- coding: utf-8 -*-
import logging

from utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    dist = 0
    depth = 0
    aim = 0

    for i in ipt:
        if 'forward' in i:
            x = int(i.replace('forward ', ''))
            dist += x
            depth += x * aim
        elif 'down' in i:
            aim += int(i.replace('down ', ''))
        elif 'up' in i:
            aim -= int(i.replace('up ', ''))

    return depth * dist

setup:
	pre-commit autoupdate
	pre-commit install

format:
	isort .
	black -v .

lint:
	pre-commit run --all-files
	flake8 .

clean:
	rm -r aoc_2021/**/__pycache__ && rm -r aoc_2020/**/__pycache__ && rm -r aoc_2019/**/__pycache__

test:
	cd aoc_2021 && python test.py
	# cd aoc_2020 && python test.py
	# cd aoc_2019 && python test.py
